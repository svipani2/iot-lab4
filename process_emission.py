import json
import logging
import sys
import pandas as pd
import numpy as np

import greengrasssdk

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")

# Counter
my_counter = 0
def lambda_handler(event, context):
    #TODO1: Get your data
    data = np.array(context['data'].split(',')).astype(float)

    #TODO2: Calculate max CO2 emission
    m = np.max(data)

    #TODO3: Return the result
    client.publish(
        topic=f"myTopic_{str(context['vehicle'])}",
        payload=json.dumps(
            {"message": str(m)}
        ),
    )

    return
